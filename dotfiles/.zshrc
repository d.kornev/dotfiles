# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
export PATH=$HOME/local/bin:$PATH

bindkey -v

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/dka/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

alias vi='vim'
alias tm='tmux'
alias vim='nvim'
# alias less='nvimpager'


autoload -U promptinit
promptinit
# prompt gentoo

PS1="%B%F{green}%n@%m%k %B%F{blue}%1~ %# %b%f%k"

KEYTIMEOUT=1
# MANPAGER="nvimpager"
# EDITOR="nvim"
# PAGER="nvimpager"
# bindkey '^R' history-incremental-search-backward

# autostart x

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

