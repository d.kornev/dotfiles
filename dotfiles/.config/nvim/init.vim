"Theme

set t_Co=256
set encoding=utf-8
colorscheme molokai

let g:python3_host_prog="/home/dka/projects/app/neovim/bin/python"
let g:loaded_python_provider = 1

call plug#begin('~/.config/nvim/plugged')

"FILEMANAGER
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'

"LINTER
Plug 'w0rp/ale'

"AUTOCOMPLETE
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi'
Plug 'zchee/deoplete-go', { 'do': 'make'}
Plug 'mattn/emmet-vim'
"Syntax
Plug 'posva/vim-vue'
"OTHeR
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

" set SPACE as LEADER key
let mapleader = "\<Space>"

" enable syntax highlighting
syntax enable

" show line numbers
set number
set colorcolumn=80
" set tabs to have 4 spaces
set ts=4

" indent when moving to the next line while writing code
set autoindent

" expand tabs into spaces
set expandtab

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" show a visual line under the cursor's current line
set cursorline

" show the matching part of the pair for [] {} and ()
set showmatch

" swap buffers without save
set hidden
" BINDINGS

" toggle numbers
map <Leader>L :set invnumber<CR>

"------  Buffer Navigation  ------
"" Ctrl Left/h & Right/l cycle between buffers
noremap <silent> <C-h> :bprev<CR>
noremap <silent> <C-l> :bnext<CR>

" <Leader>q Closes the current buffer
"nnoremap <silent> <Leader>q :Bclose<CR>
" map <leader>q :bd<CR>
" Super dupper close last buffer without window kill
map <leader>q :bp<bar>sp<bar>bn<bar>bd<CR>

" remove search highlight
nnoremap <silent> <leader>z :nohlsearch<CR>

" remove trailing whitespaces on save
autocmd BufWritePre * %s/\s\+$//e

"  при переходе за границу в 80 символов в Ruby/Python/js/C/C++ подсвечиваем на темном фоне текст
 augroup vimrc_autocmds
     autocmd!
     autocmd FileType ruby,python,javascript,c,cpp,php highlight Excess ctermbg=DarkGrey guibg=Black
     autocmd FileType ruby,python,javascript,c,cpp,php match Excess /\%80v.*/
     autocmd FileType ruby,python,javascript,c,cpp,php set nowrap
 augroup END
" Filetypes
"autocmd Filetype html setlocal ts=2 sts=2 sw=2 autoindent
autocmd Filetype javascript setlocal ts=2 sw=2 sts=2 autoindent smartindent

" NERDTree
"

map <F3> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$']

"copy/paste mode
set pastetoggle=<F8>

" TagBar настройки
map <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 0 " автофокус на Tagbar при открытии

" ALE
"

let g:ale_lint_on_text_changed = 'never'
"let g:ale_linters = {'python': ['pycodestyle']}
let g:ale_linters = {
            \'python': ['flake8'],
            \'javascript': ['eslint']
            \}

" DEOPLETE
"

let g:deoplete#enable_at_startup = 1

" JEDI
"
autocmd FileType python setlocal completeopt-=preview

"airline
"
let g:airline#extensions#tabline#enabled = 1

let g:airline#extensions#tabline#formatter = 'unique_tail'
" let g:airline_powerline_fonts = 1

" SHOTRCUTS
"
iab ifmain! if __name__ == '__main__':
map <leader>b <S-O>import pdb; pdb.set_trace()<ESC>
