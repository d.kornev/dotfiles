```
dotfiles
├── .config
│   ├── i3
│   │   └── config
│   ├── i3status
│   │   └── config
│   └── nvim
│       ├── colors
│       │   └── molokai.vim
│       └── init.vim
├── .tmux.conf
├── .Xdefaults
└── .zshrc

5 directories, 7 files
```
