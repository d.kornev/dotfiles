#!/usr/bin/env sh

PROJ_DIR=`pwd`"/dotfiles/"

MY_FILES="
.tmux.conf
.zshrc
.Xdefaults
.config/i3/config
.config/i3status/config
.config/nvim/init.vim
.config/nvim/colors/molokai.vim
"

cd $HOME

for i in $MY_FILES;
do echo "copying" $i to $PROJ_DIR  && cp --parents $i $PROJ_DIR ;
done

echo "DONE"
